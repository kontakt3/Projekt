/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Additional;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.sql.Date;

/**
 * Ease of access for changing dates by a given amount
 *
 * @author Michael Overgaard
 */
public class DateUtil {

    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static Date addDays(Date date, int days) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.clear();
            calendar.setTime(date);

            calendar.add(Calendar.DATE, 2);

            java.sql.Date newDate;
            newDate = new java.sql.Date(calendar.getTime().getTime());

            return newDate;

        } catch (Exception e) {
            System.out.println("Error in subtract\n" + e);
            return date;
        }
    }

    public static Date subtractDays(Date date, int days) {
        try {
            days = days - (days * 2);

            Calendar calendar = Calendar.getInstance();
            calendar.clear();
            calendar.setTime(date);

            calendar.add(Calendar.DATE, days);

            java.sql.Date newDate;
            newDate = new java.sql.Date(calendar.getTime().getTime());

            return newDate;

        } catch (Exception e) {
            System.out.println("Error in subtract\n" + e);
            return date;
        }

    }

}
