/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

/**
 *
 * @author Andreas Fisker
 */
public class Warehouse {

    private int wNo;
    private int RNO;
    private int amount;

    public Warehouse() {
    }

    public Warehouse(int wNo, int RNO, int amount) {
        this.wNo = wNo;
        this.RNO = RNO;
        this.amount = amount;
    }

    public int getStockNo() {
        return wNo;
    }

    public void setStockNo(int stockNo) {
        this.wNo = stockNo;
    }

    public int getRNO() {
        return RNO;
    }

    public void setRNO(int RNO) {
        this.RNO = RNO;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

}
