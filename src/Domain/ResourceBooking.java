/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

/**
 *
 * @author Michael Overgaard
 */
public class ResourceBooking {

    private int amount;
    private int RNO;
    private int WNO;
    private int ONO;

    public ResourceBooking(int amount, int RNO, int WNO, int ONO) {
        this.amount = amount;
        this.RNO = RNO;
        this.WNO = WNO;
        this.ONO = ONO;
    }

    @Override
    public String toString() {
        return "ResourceBooking{" + "amount=" + amount + ", RNO=" + RNO + ", WNO=" + WNO + ", ONO=" + ONO + '}';
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getRNO() {
        return RNO;
    }

    public void setRNO(int RNO) {
        this.RNO = RNO;
    }

    public int getWNO() {
        return WNO;
    }

    public void setWNO(int WNO) {
        this.WNO = WNO;
    }

    public int getONO() {
        return ONO;
    }

    public void setONO(int ONO) {
        this.ONO = ONO;
    }

}
