/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

/**
 *
 * @author Andreas Fisker
 */
public class Customer {

    private int CNO;
    private String customerName;
    private String email;
    private String phoneNo;
    private String address;

    public Customer() {
    }

    public Customer(int CNO, String customerName, String email, String phoneNo, String address) {
        this.CNO = CNO;
        this.customerName = customerName;
        this.email = email;
        this.phoneNo = phoneNo;
        this.address = address;
    }

    public int getCNO() {
        return CNO;
    }

    public void setCNO(int CNO) {
        this.CNO = CNO;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
