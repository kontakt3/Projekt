/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;
//Fisker

public class RessourceType {

    private int RNO;
    private String ressourceName;
    private int rValue;
    private int amount;

    public RessourceType() {
    }

    public RessourceType(int RNO, String ressourceName, int rValue, int amount) {
        this.RNO = RNO;
        this.ressourceName = ressourceName;
        this.rValue = rValue;
        this.amount = amount;
    }

    public int getRNO() {
        return RNO;
    }

    public void setRNO(int RNO) {
        this.RNO = RNO;
    }

    public String getRessourceName() {
        return ressourceName;
    }

    public void setRessourceName(String ressourceName) {
        this.ressourceName = ressourceName;
    }

    public int getrValue() {
        return rValue;
    }

    public void setrValue(int rValue) {
        this.rValue = rValue;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

}
