package Domain;

import dataSource.DBFacade;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controller {

    private boolean processingOrder;
    private boolean processingRessourceType;
    private boolean processingTeam;
    private boolean processingCostumer;
    private Order currentOrder;
    private DBFacade dbFacade;
    private RessourceType currentRessourceType;
    private Team currentTeam;
    private Customer currentCustomer;

    private static Controller instance;

    public Controller() throws SQLException {
        processingOrder = false;
        processingTeam = false;
        processingCostumer = false;
        currentOrder = null;
        currentRessourceType = null;
        dbFacade = DBFacade.getInstance();
        currentTeam = null;
        currentCustomer = null;
    }
//laver en instans af controlleren efter singleton mønster.

    public static Controller getInstance() throws SQLException {
        if (instance == null) {
            instance = new Controller();
        }
        return instance;
    }
//Her hentes en order

    public Order getOrder(int ono) {
        if (processingOrder) {
            return null;
        }

        dbFacade.startNewBusinessTransaction();
        processingOrder = true;
        currentOrder = dbFacade.getOrder(ono);

        // Kun med for at få programmet til at køre for nu. Uden den så går det i stå når man prøver at loade en anden ordre
        processingOrder = false;

        return currentOrder;
    }

    //Her hentes alle orders
    public ArrayList<Order> getAllOrders() {
        return dbFacade.getAllOrders();
    }
//Metod til at slette en order

    public boolean deleteOrder(int ono) {
        boolean orderDeleted = false;

        if (processingOrder) {
            return orderDeleted;
        }

        processingOrder = true;
        dbFacade.startNewBusinessTransaction();

        try {
            dbFacade.deleteOrder(ono);
        } catch (SQLException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("fail in controller->dbFacade.deleteOrder()");
            return false;
        }

        orderDeleted = saveOrder();

        return orderDeleted;
    }
//Metode til at update en order

    public boolean updateOrder(int ono, int cno, String deposit, int depositPrice, int v) {

        boolean orderUpdated = false;

        if (processingOrder) {
            return orderUpdated;
        }

        dbFacade.startNewBusinessTransaction();

        processingOrder = true;

        currentOrder = new Order(ono, deposit, depositPrice, cno, "", "");
//        currentOrder.setONO(ono);
//        currentOrder.setDeposit(deposit);
//        currentOrder.setDepositPrice(depositPrice);
//        currentOrder.setCNO(cno);
        currentOrder.setVersion(v);

        dbFacade.registerDirtyOrder(currentOrder);

        orderUpdated = true;

        return orderUpdated;
    }
//Metode til at lave en ny order

    public int createNewOrder(String deposit, int depositPrice, int CNO, String startDate, String endDate) {

        boolean orderCreated = false;

        if (processingOrder) {
            return -13;
        }

        dbFacade.startNewBusinessTransaction();
        int newOrderNo = dbFacade.getNextOrderNo();// DB-generated unique ID
        if (newOrderNo != 0) {
            processingOrder = true;

            currentOrder = new Order(newOrderNo, deposit, depositPrice, CNO, startDate, endDate);
            dbFacade.registerNewOrder(currentOrder);
            orderCreated = true;
        } else {
            processingOrder = false;
            currentOrder = null;
        }

        processingOrder = false;
        return newOrderNo;
    }
//Metode til at oprette et team

    public int createNewTeam(String teamName, int truckNO) {

        int TNO = dbFacade.getNextTeamNo();

        boolean teamCreated = false;

        dbFacade.startNewBusinessTransaction();
        processingTeam = true;

        currentTeam = new Team(TNO, teamName, truckNO);
        dbFacade.registerNewTeam(currentTeam);
        currentTeam = null;

        teamCreated = saveTeam();
        return TNO;
    }
//Metode til at update/redigere team

    public boolean updateTeam(int TNO, String TEAMNAME, int TruckNo) {

        boolean teamUpdated = false;

        if (processingTeam) {
            return teamUpdated;
        }

        dbFacade.startNewBusinessTransaction();

        processingTeam = true;

        currentTeam = new Team(TNO, TEAMNAME, TruckNo);
        dbFacade.registerDirtyTeam(currentTeam);

        teamUpdated = saveTeam();
        return teamUpdated;
    }
//Metode til at hente alle teams

    public ArrayList<Team> getAllTeams() {
        return dbFacade.getAllTeams();
    }

    //metode til at delete team
    public boolean deleteTeam(int tno) {
        boolean teamDeleted = false;

        if (processingTeam) {
            return teamDeleted;
        }

        processingTeam = true;
        dbFacade.startNewBusinessTransaction();

        try {
            dbFacade.deleteTeam(tno);
        } catch (SQLException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("fail in controller->dbFacade.deleteTeam()");
            return false;
        }

        teamDeleted = saveTeam();

        return teamDeleted;
    }
//Metode til at oprette en ny customer

    public int createNewCostumer(String customerName, String email, String phone, String address) {

        int CNO = dbFacade.getNextCustomerNo();
        boolean costumerCreated = false;
        System.out.println(CNO);
        if (processingCostumer) {
            return -13;
        }

        dbFacade.startNewBusinessTransaction();
        //int newOrderNo = dbFacade.getNextOrderNo();// DB-generated unique ID
        // if (newOrderNo != 0) {
        processingCostumer = true;

        currentCustomer = new Customer(CNO, customerName, email, phone, address);
        dbFacade.registerNewCustomer(currentCustomer);

        currentCustomer = null;

        costumerCreated = true;
        return CNO;
    }
//Metode til at redigere/update customer

    public boolean updateCustomer(int CNO, String customerName, String email, String phone, String address) {

        boolean custumerUpdated = false;

        if (processingCostumer) {
            return custumerUpdated;
        }

        dbFacade.startNewBusinessTransaction();

        processingCostumer = true;

        currentCustomer = new Customer(CNO, customerName, email, phone, address);
        dbFacade.registerDirtyCustomer(currentCustomer);

        custumerUpdated = saveCustomer();
        return custumerUpdated;
    }
//Metode til at delete customer

    public boolean deleteCustomer(int CNO) {
        boolean customerDeleted = false;

        if (processingCostumer) {
            return customerDeleted;
        }

        processingCostumer = true;
        dbFacade.startNewBusinessTransaction();

        try {
            dbFacade.deleteCustomer(CNO);
        } catch (SQLException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("fail in controller->dbFacade.deleteCustomer()");
            return false;
        }

        customerDeleted = saveCustomer();

        return customerDeleted;
    }

    //Metode til at hente alle customers
    public ArrayList<Customer> getAllCustomers() {
        return dbFacade.getAllCustomers();
    }
//Metode til at tilføje resource til et order.

    public void addResourceBooking(int amount, int RNO, int WNO, int ONO) {
        ResourceBooking or = new ResourceBooking(amount, RNO, WNO, ONO);
        dbFacade.registerNewResourceBooking(or);
    }
//Metode til at delete resources til en order

    public void deleteResourceBooking(int RNO, int ONO) {
        ResourceBooking or = new ResourceBooking(500, RNO, 1, ONO);
        dbFacade.registerResourceBookingForDeletion(null);
        saveOrder();
    }
//Metode til at check om resources er ledig.

    public boolean checkAva(int rno, double amount, Date startDate, Date endDate) {
        boolean check;
        check = dbFacade.checkAva(rno, amount, startDate, endDate);
        return check;
    }
//Metode til at oprette en ny resource

    public int createNewRessourceType(String ressourceName, int rValue, int amount) {

        int RNO = dbFacade.getNextRessourceTypeNo();
        boolean ressourceCreated = false;

        dbFacade.startNewBusinessTransaction();
        processingRessourceType = true;

        currentRessourceType = new RessourceType(RNO, ressourceName, rValue, amount);
        dbFacade.registerNewRessourceType(currentRessourceType);

        ressourceCreated = saveRessourceType();
        return RNO;
    }
//Metode til at update/redigere en resource

    public boolean updateRessourceType(int rno, String ressourceName, int rValue, int amount) {

        boolean ressourceTypeUpdated = false;

        if (processingRessourceType) {
            return ressourceTypeUpdated;
        }

        dbFacade.startNewBusinessTransaction();

        processingRessourceType = true;

        currentRessourceType = new RessourceType(rno, ressourceName, rValue, amount);
        dbFacade.registerDirtyRessourceType(currentRessourceType);

        ressourceTypeUpdated = saveRessourceType();
        return ressourceTypeUpdated;
    }
//metode til at hente alle resource

    public ArrayList<RessourceType> getAllRessources() {
        return dbFacade.getAllRessources();
    }

    //Metode til at slette en resource
    public boolean deleteRessourceType(int rno) {
        boolean ressourceTypeDeleted = false;

        if (processingRessourceType) {
            return ressourceTypeDeleted;
        }

        processingRessourceType = true;
        dbFacade.startNewBusinessTransaction();

        try {
            dbFacade.deleteRessourceType(rno);
        } catch (SQLException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("fail in controller->dbFacade.deleteOrder()");
            return false;
        }

        ressourceTypeDeleted = saveRessourceType();

        return ressourceTypeDeleted;
    }
//Metode til at gemme en order i databasen

    public boolean saveOrder() {
        boolean status = false;

        status = dbFacade.commitBusinessTransaction();
        currentOrder = null;
        return status;
    }
//Metode til at gemme et team i databasen

    public boolean saveTeam() {
        boolean status = false;

        status = dbFacade.commitBusinessTransaction();
        processingTeam = false;
        currentTeam = null;
        return status;
    }
//Metode til at gemme en customer i databasen

    public boolean saveCustomer() {
        boolean status = false;
        status = dbFacade.commitBusinessTransaction();
        processingCostumer = false;
        currentCustomer = null;

        return status;
    }
//metode til at gemme en resource i databasen

    public boolean saveRessourceType() {
        boolean status = false;
        status = dbFacade.commitBusinessTransaction();
        processingRessourceType = false;
        currentRessourceType = null;

        return status;
    }
//metode til at nulstille den order der redigeres i 

    public void resetOrder() {
        processingOrder = false;
        currentOrder = null;
    }
//metode til printliste

    public String getPrintList(int ono) {
        String printString = "OrderNo: " + ono + "\n\n" + "From warehouse no. 1:\n\n";

        //getting arraylist from DBFacade
        ArrayList<String[]> printArray = dbFacade.getPrintList(ono);
        boolean addWarehouse2 = false;
        //Arranging the String
        for (String[] sA : printArray) {
            if (sA[4].equalsIgnoreCase("1")) {
                printString = printString.concat(
                        "RNO: " + sA[1] + "\n" + sA[3] + " " + sA[2] + "\n\n"
                );
            } else if (sA[4].equalsIgnoreCase("2")) {
                addWarehouse2 = true;
            }
        }
        if (addWarehouse2) {
            printString = printString.concat("\nFrom warehouse no. 2\n\n");

            for (String[] sA : printArray) {
                if (sA[4].equalsIgnoreCase("2")) {
                    printString = printString.concat(
                            "RNO: " + sA[1] + "\n" + sA[3] + " " + sA[2] + "\n\n"
                    );
                }
            }
        }

        return printString;
    }
}
