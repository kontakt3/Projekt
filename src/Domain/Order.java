/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import Additional.DateUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Andreas Fisker & Michael Overgaard
 */
public class Order {

    // standardized amount of days it takes to setup and dismantle the scaffolding
    private final int timeToBuildOrDismantle = 2;

    private int ONO;
    private int CNO;
    private String deposit;
    private int depositPrice;
    private Date setupStartDate;
    private Date setupEndDate;              //De to datoer som kunden 'angiver': setupEndDate, dismantligStartDate
    private Date dismantlingStartDate;      // -------  de andre to bliver udregnet ud fra disse
    private Date dismantlingEndDate;
    private int version;

    public Order(int ONO, String deposit, int depositPrice, int CNO, String startDate, String endDate) {
        this.ONO = ONO;
        this.CNO = CNO;
        this.deposit = deposit;
        this.depositPrice = depositPrice;

        try {
            this.setupEndDate = Date.valueOf(startDate);
            this.dismantlingStartDate = Date.valueOf(endDate);

            this.setupStartDate = DateUtil.subtractDays(setupEndDate, timeToBuildOrDismantle);
            this.dismantlingEndDate = DateUtil.addDays(dismantlingStartDate, timeToBuildOrDismantle);
        } catch (Exception e) {
            System.out.println("No valid date entered");
        }
    }

    public Order(int ono, String deposit, int depositPrice, Date setupStartDate, Date setupEndDate, Date dismantlingStartDate, Date dismantlingEndDate, int cno, int version) {

        this.ONO = ono;
        this.CNO = cno;
        this.deposit = deposit;
        this.depositPrice = depositPrice;
        this.setupStartDate = setupStartDate;
        this.setupEndDate = setupEndDate;
        this.dismantlingStartDate = dismantlingStartDate;
        this.dismantlingEndDate = dismantlingEndDate;
        this.version = version;
    }

    public void incrVer() {
        version++;
    }

    @Override
    public String toString() {
        return "Order{" + "ONO=" + ONO + ", CNO=" + CNO + ", deposit=" + deposit + ", depositPrice=" + depositPrice + ", setupStartDate=" + setupStartDate + ", setupEndDate=" + setupEndDate + ", dismantlingStartDate=" + dismantlingStartDate + ", dismantlingEndDate=" + dismantlingEndDate + ", version=" + version + '}';
    }

    public int getONO() {
        return ONO;
    }

    public void setONO(int ONO) {
        this.ONO = ONO;
    }

    public int getCNO() {
        return CNO;
    }

    public void setCNO(int CNO) {
        this.CNO = CNO;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public int getDepositPrice() {
        return depositPrice;
    }

    public void setDepositPrice(int depositPrice) {
        this.depositPrice = depositPrice;
    }

    public Date getSetupStartDate() {
        return setupStartDate;
    }

    public Date getSetupEndDate() {
        return setupEndDate;
    }

    public Date getDismantlingStartDate() {
        return dismantlingStartDate;
    }

    public Date getDismantlingEndDate() {
        return dismantlingEndDate;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
