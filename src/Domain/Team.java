/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

/**
 *
 * @author Andreas Fisker
 */
public class Team {

    private int TNO;
    private String teamName;
    private int truckNO;

    public Team(int TNO, String teamName, int truckNO) {
        this.TNO = TNO;
        this.teamName = teamName;
        this.truckNO = truckNO;
    }

    public Team() {
    }

    public int getTNO() {
        return TNO;
    }

    public void setTNO(int TNO) {
        this.TNO = TNO;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public int getTruckNO() {
        return truckNO;
    }

    public void setTruckNO(int truckNO) {
        this.truckNO = truckNO;
    }

}
