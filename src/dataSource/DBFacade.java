package dataSource;

import Domain.Customer;
import Domain.Order;
import Domain.ResourceBooking;
import Domain.RessourceType;
import Domain.Team;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

//==	Provides a facade to the Data Source Layer
//	Encapsulates connection handling
//	Made Singleton to restrict the number of Unit of Work objects to 1.
//	2014/hau
public class DBFacade {

    private UnitOfWorkProcessOrder uow;
    private Connection con;
    //=====	Singleton
    private static DBFacade instance;

    private DBFacade() throws SQLException {
        con = DBConnector.getInstance().getConnection();
        uow = new UnitOfWorkProcessOrder();
    }
//Laver en instans af DBFacade i singleton mønster

    public static DBFacade getInstance() throws SQLException {
        if (instance == null) {
            instance = new DBFacade();
        }
        return instance;
    }

    // Metoder til at hente data
    public Order getOrder(int ono) {
        Order o = null;
        o = new OrderMapper().getOrder(ono, con);
        return o;
    }

    public ArrayList<Order> getAllOrders() {
        ArrayList<Order> orderList = null;
        try {
            OrderMapper om = new OrderMapper();
            orderList = om.getAllOrders(con);
        } catch (Exception e) {
            System.out.println("fail in dbFacade.getAllOrders()");
            System.out.println(e);
        }
        return orderList;
    }

    public ArrayList<RessourceType> getAllRessources() {
        ArrayList<RessourceType> ressourceList = null;
        try {
            OrderMapper om = new OrderMapper();
            ressourceList = om.getAllRessources(con);
        } catch (Exception e) {
            System.out.println("fail in dbFacade.getAllRessources()");
            System.out.println(e);
        }
        return ressourceList;
    }

    public ArrayList<Team> getAllTeams() {
        ArrayList<Team> teamList = null;
        try {
            OrderMapper om = new OrderMapper();
            teamList = om.getAllTeams(con);
        } catch (Exception e) {
            System.out.println("fail in dbFacade.getAllTeams()");
            System.out.println(e);
        }
        return teamList;
    }
//checker om resourcer er ledige

    public boolean checkAva(int rno, double amount, Date startDate, Date endDate) {
        boolean o = new OrderMapper().checkAva(con, rno, amount, startDate, endDate);
        return o;
    }

    public void deleteOrder(int ono) throws SQLException {
        if (uow != null) {
            uow.registerOrderForDeletion(ono);
        }
    }

    public void deleteTeam(int tno) throws SQLException {
        if (uow != null) {
            uow.registerTeamForDeletion(tno);
        }
    }

    public int getNextOrderNo() {
        int nextOno = 0;
        nextOno = new OrderMapper().getNextOrderNo(con);
        return nextOno;
    }

    public int getNextCustomerNo() {
        int nextCno = 0;
        nextCno = new OrderMapper().getNextCustomerNo(con);
        return nextCno;
    }

    public int getNextTeamNo() {
        int nextTno = 0;
        nextTno = new OrderMapper().getNextTeamNo(con);
        return nextTno;
    }

    public int getNextRessourceTypeNo() {
        int nextRessourceTypeNo = 0;
        nextRessourceTypeNo = new OrderMapper().getNextRessourceTypeNo(con);
        return nextRessourceTypeNo;
    }

    public ArrayList<String[]> getPrintList(int ono) {
        return new OrderMapper().getPrintList(ono, con);
    }

    public boolean updateTeam(ArrayList<Team> array) throws SQLException {

        boolean t = false;
        t = new OrderMapper().updateTeam(array, con);
        return true;
    }

//Metoder til at registrere ændringer i UnitOfWorkProcessOrder
    public void registerNewOrder(Order o) {
        if (uow != null) {
            uow.registerNewOrder(o);
        }
    }

    public void registerNewTeam(Team t) {
        if (uow != null) {
            uow.registerNewTeam(t);
        }
    }

    public void registerNewCustomer(Customer c) {
        if (uow != null) {
            uow.registerNewCustomer(c);
        }
    }

    public void registerDirtyCustomer(Customer c) {
        if (uow != null) {
            uow.registerDirtyCustomer(c);
        }
    }

    public void deleteCustomer(int CNO) throws SQLException {
        if (uow != null) {
            uow.registerCustomerForDeletion(CNO);
        }
    }

    public ArrayList<Customer> getAllCustomers() {
        OrderMapper om = new OrderMapper();
        try {
            return om.getAllCustomers(con);
        } catch (SQLException ex) {
            Logger.getLogger(DBFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void registerDirtyOrder(Order o) {
        if (uow != null) {
            uow.registerDirtyOrder(o);
        }
    }

    public void registerDirtyTeam(Team T) {
        if (uow != null) {
            uow.registerDirtyTeam(T);
        }
    }

    public void registerNewRessourceType(RessourceType r) {
        if (uow != null) {
            uow.registerNewRessourceType(r);
        }
    }

    public void registerDirtyRessourceType(RessourceType r) {
        if (uow != null) {
            uow.registerDirtyRessourceType(r);
        }
    }

    public void deleteRessourceType(int rno) throws SQLException {
        if (uow != null) {
            uow.registerRessourceTypeForDeletion(rno);
        }
    }

    public void registerNewResourceBooking(ResourceBooking or) {
        if (uow != null) {
            uow.registerNewResourceBooking(or);
        }
    }

    public void registerDirtyResourceBooking(ResourceBooking or) {
        if (uow != null) {
            uow.registerDirtyResourceBooking(or);
        }
    }

    public void registerResourceBookingForDeletion(ResourceBooking or) {
        if (uow != null) {
            uow.registerResourceBookingForDeletion(or);
        }
    }

    //Metoder til at håndtere business transactions
    public void startNewBusinessTransaction() {
        uow = new UnitOfWorkProcessOrder();
    }

    //Gemmer alle ændringer
    public boolean commitBusinessTransaction() {
        boolean status = false;
        if (uow != null) {
            try {
                status = uow.commit(con);
            } catch (Exception e) {
                System.out.println("Fail in DBFacade - commitBusinessTransaction");
                System.err.println(e);
            }
            uow = null;
        }
        return status;
    }

}
