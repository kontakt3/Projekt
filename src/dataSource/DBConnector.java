package dataSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnector {

    private static DBConnector instance;
    Statement statement = null;
    Connection connection = null;

    public DBConnector() {
        try {
            Class.forName(DB.driver);
            connection = DriverManager.getConnection(DB.URL, DB.ID, DB.PW);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
//Oprette en instans af DBconnector i singleton mønster

    public static DBConnector getInstance() {
        if (instance == null) {
            instance = new DBConnector();
        }
        return instance;
    }
//Metode der returnere den instatierede connection

    public Connection getConnection() throws SQLException {
        System.out.println("Connected to database");
        return connection;
    }

}
