package dataSource;

import Domain.Customer;
import java.util.ArrayList;
import Domain.Order;
import Domain.ResourceBooking;
import Domain.RessourceType;
import Domain.Team;
import java.sql.Connection;
import java.sql.SQLException;

public class UnitOfWorkProcessOrder {

    static boolean testRun = true;

    private ArrayList<RessourceType> newRessourceTypes;
    private ArrayList<RessourceType> dirtyRessourceTypes;
    private ArrayList<ResourceBooking> newResourceBooking;
    private ArrayList<ResourceBooking> dirtyResourceBooking;
    private ArrayList<Customer> newCustomer;
    private ArrayList<Customer> dirtyCustomer;
    private ArrayList<Order> newOrders;
    private ArrayList<Order> dirtyOrders;
    private ArrayList<Team> newTeam;
    private ArrayList<Team> dirtyTeam;

    private ArrayList<ResourceBooking> resourceBookingForDeletion;

    private int ressourceTypeForDeletion;
    private int customerForDeletion;
    private int orderForDeletion;
    private int teamForDeletion;

    public UnitOfWorkProcessOrder() {
        newOrders = new ArrayList<>();
        dirtyOrders = new ArrayList<>();
        newRessourceTypes = new ArrayList<>();
        dirtyRessourceTypes = new ArrayList<>();
        newResourceBooking = new ArrayList<>();
        dirtyResourceBooking = new ArrayList<>();
        resourceBookingForDeletion = new ArrayList<>();
        newCustomer = new ArrayList<>();
        dirtyCustomer = new ArrayList<>();
        newTeam = new ArrayList<>();
        dirtyTeam = new ArrayList<>();
        orderForDeletion = 0;
        teamForDeletion = 0;
        customerForDeletion = 0;
        ressourceTypeForDeletion = 0;
    }
//Registrere ny order

    public void registerNewOrder(Order o) {
        newOrders.add(o);
    }
//Resgistrere en dirty order

    public void registerDirtyOrder(Order o) {
        if (!newOrders.contains(o)
                && !dirtyOrders.contains(o)
                && orderForDeletion == 0) {
            dirtyOrders.add(o);
        }
    }
//Registrere et dirty team

    public void registerDirtyTeam(Team t) {
        if (!newTeam.contains(t)
                && !dirtyTeam.contains(t)
                && teamForDeletion == 0) {
            dirtyTeam.add(t);
        }
    }
//Registrere ny resource

    public void registerNewRessourceType(RessourceType r) {
        if (!newRessourceTypes.contains(r)
                && !dirtyRessourceTypes.contains(r)
                && ressourceTypeForDeletion == 0) {
            newRessourceTypes.add(r);
        }
    }
//Registrere en ny dirty resource

    public void registerDirtyRessourceType(RessourceType r) {
        if (!newRessourceTypes.contains(r)
                && !dirtyRessourceTypes.contains(r)
                && ressourceTypeForDeletion == 0) {
            dirtyRessourceTypes.add(r);
        }
    }
//Registrere en resource der skal slettes

    public void registerRessourceTypeForDeletion(int rno) {

        if (newRessourceTypes.isEmpty()
                && dirtyRessourceTypes.isEmpty()
                && ressourceTypeForDeletion == 0) {
            ressourceTypeForDeletion = rno;
        }
    }
//Registrere en order der skal slettes

    public void registerOrderForDeletion(int ono) {

        if (newOrders.isEmpty()
                && dirtyOrders.isEmpty()
                && orderForDeletion == 0) {
            orderForDeletion = ono;
        }
    }
//Registrere et team der skal slettes

    public void registerTeamForDeletion(int tno) {

        if (newTeam.isEmpty()
                && dirtyTeam.isEmpty()
                && teamForDeletion == 0) {
            teamForDeletion = tno;
        }
    }
//Registrere en resource tilføjes til en order

    public void registerNewResourceBooking(ResourceBooking or) {
        newResourceBooking.add(or);
    }
//Registrere en dirty resource tilføjes til en order

    public void registerDirtyResourceBooking(ResourceBooking or) {
        dirtyResourceBooking.add(or);
    }
//Registrere en resource der skal slettes

    public void registerResourceBookingForDeletion(ResourceBooking or) {
        resourceBookingForDeletion.add(or);
    }
//Registrere et nyt team

    public void registerNewTeam(Team t) {
        if (!newTeam.contains(t)
                && !dirtyTeam.contains(t)
                && teamForDeletion == 0) {
            newTeam.add(t);
        }
    }
//Registrere en ny customer

    public void registerNewCustomer(Customer c) {
        if (!newCustomer.contains(c)
                && !dirtyCustomer.contains(c)
                && customerForDeletion == 0) {
            newCustomer.add(c);
        }
    }
//Registrere en dirty customer

    public void registerDirtyCustomer(Customer c) {
        dirtyCustomer.add(c);
    }
//Registrere en customer der skal slettes

    public void registerCustomerForDeletion(int CNO) {

        if (newCustomer.isEmpty()
                && dirtyCustomer.isEmpty()
                && customerForDeletion == 0) {
            customerForDeletion = CNO;
        }
    }
//Commit metoden der committer til databasen

    public boolean commit(Connection conn) throws SQLException {
        boolean status = true;
        try {

            conn.setAutoCommit(false);
            OrderMapper om = new OrderMapper();
            status = status && om.createCustomer(newCustomer, conn);
            status = status && om.insertTeam(newTeam, conn);
            status = status && om.createResourceType(newRessourceTypes, conn);
            status = status && om.insertOrders(newOrders, conn);
            status = status && om.createResourceBooking(newResourceBooking, conn);

            status = status && om.updateResourceBooking(dirtyResourceBooking, conn);
            status = status && om.updateOrders(dirtyOrders, conn);
            status = status && om.updateResourceType(dirtyRessourceTypes, conn);
            status = status && om.updateTeam(dirtyTeam, conn);
            status = status && om.updateCustomer(dirtyCustomer, conn);

            status = status && om.deleteResourceBooking(resourceBookingForDeletion, conn);
            status = status && om.deleteOrder(orderForDeletion, conn);
            status = status && om.deleteResourceType(ressourceTypeForDeletion, conn);
            status = status && om.deleteTeam(teamForDeletion, conn);
            status = status && om.deleteCustomer(customerForDeletion, conn);

            if (!status) {
                throw new Exception("Business Transaction aborted");
            }

            conn.commit();

        } catch (Exception e) {
            System.out.println("fail in UnitOfWork - commit()");
            System.err.println(e);
            conn.rollback();
            status = false;

        }
        newOrders = new ArrayList<>();
        dirtyOrders = new ArrayList<>();
        newRessourceTypes = new ArrayList<>();
        dirtyRessourceTypes = new ArrayList<>();
        newResourceBooking = new ArrayList<>();
        dirtyResourceBooking = new ArrayList<>();
        resourceBookingForDeletion = new ArrayList<>();
        newCustomer = new ArrayList<>();
        dirtyCustomer = new ArrayList<>();
        newTeam = new ArrayList<>();
        dirtyTeam = new ArrayList<>();
        orderForDeletion = 0;
        teamForDeletion = 0;
        customerForDeletion = 0;
        ressourceTypeForDeletion = 0;

        return status;
    }

}
