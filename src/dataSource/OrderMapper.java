package dataSource;

import Domain.Customer;
import java.sql.*;
import java.util.ArrayList;
import Domain.Order;
import Domain.ResourceBooking;
import Domain.RessourceType;
import Domain.Team;

public class OrderMapper {

    static boolean testRun = true;
//Metode til at genere SQL Statement når man opretter en order

    public boolean insertOrders(ArrayList<Order> listOfOrders, Connection conn) throws SQLException {
        int rowsInserted = 0;
        String SQLString = "insert into orders "
                + "(ono, deposite, depositeprice, setupstartdate, setupenddate, dismantlingstartdate, dismantlingenddate, cno) "
                + "values (?,?,?,?,?,?,?,?) ";
        try {
            PreparedStatement statement = null;
            statement = conn.prepareStatement(SQLString);

            for (Order o : listOfOrders) {
                statement.setInt(1, o.getONO());
                statement.setString(2, o.getDeposit());
                statement.setInt(3, o.getDepositPrice());
                statement.setDate(4, o.getSetupStartDate());
                statement.setDate(5, o.getSetupEndDate());
                statement.setDate(6, o.getDismantlingStartDate());
                statement.setDate(7, o.getDismantlingEndDate());
                statement.setInt(8, o.getCNO());
                rowsInserted += statement.executeUpdate();
            }
        } catch (Exception e) {
            System.out.println("Error in om.insertOrders()");
            System.out.println(e);
        }
        return (rowsInserted == listOfOrders.size());
    }
//Metode til at genere SQL Statement når man redigere en order

    public boolean updateOrders(ArrayList<Order> listOfOrders, Connection conn) throws SQLException {
        int rowsUpdated = 0;
        String SQLString = "update orders "
                + "set ONO = ?, DEPOSITE = ?, DEPOSITEPRICE = ?, CNO = ?, V = ? "
                + "where ONO = ? and V = ?";
        try {
            PreparedStatement statement = null;
            statement = conn.prepareStatement(SQLString);

            for (Order o : listOfOrders) {
                statement.setInt(1, o.getONO());
                statement.setString(2, o.getDeposit());
                statement.setInt(3, o.getDepositPrice());
                statement.setInt(4, o.getCNO());
                statement.setInt(5, o.getVersion() + 1);
                statement.setInt(6, o.getONO());
                statement.setInt(7, o.getVersion());
                int tupleUpdated = statement.executeUpdate();
                if (tupleUpdated == 1) {
                    o.incrVer();
                }
                rowsUpdated += tupleUpdated;

            }

        } catch (Exception e) {
            System.out.println("fail in updateOrder - blah blah blah");
            System.out.println(e);

        }

        return (rowsUpdated == listOfOrders.size());
    }
//Metode til at genere SQL Statement når man sletter en order

    public boolean deleteOrder(int ono, Connection conn) throws SQLException {

        String SQLString1 = "DELETE FROM RESOURCEBOOKINGS WHERE ONO = ?";
        PreparedStatement statement = null;

        try {
            //delete order ressource
            statement = conn.prepareStatement(SQLString1);
            statement.setInt(1, ono);
            statement.execute();

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - deleteOrder - del 1");
            System.out.println(e);
            return false;
        }

        String SQLString2 = "DELETE FROM ORDERS WHERE ONO = ?";
        PreparedStatement statement2 = null;

        try {
            //delete order
            statement2 = conn.prepareStatement(SQLString2);
            statement2.setInt(1, ono);
            statement2.execute();

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - deleteOrder del 2");
            System.out.println(e);
            return false;
        }

        return true;
    }
//Metode til at genere SQL Statement når man opretter en resource

    public boolean createResourceType(ArrayList<RessourceType> listOfRessourceTypes, Connection conn) throws SQLException {
        int rowsInserted = 0;
        String SQLString = "insert into ressourcetypes values (?,?,?)";
        PreparedStatement statement = null;
        statement = conn.prepareStatement(SQLString);

        for (RessourceType r : listOfRessourceTypes) {
            statement.setInt(1, r.getRNO());
            statement.setString(2, r.getRessourceName());
            statement.setInt(3, r.getrValue());
            rowsInserted += statement.executeUpdate();
        }

        SQLString = "insert into WAREHOUSE values (?, ?, ?)";
        statement = conn.prepareStatement(SQLString);

        for (RessourceType r : listOfRessourceTypes) {
            statement.setInt(1, 1);
            statement.setInt(2, r.getRNO());
            statement.setInt(3, r.getAmount());
            statement.execute();
        }

        return (rowsInserted == listOfRessourceTypes.size());
    }
//Metode til at genere SQL Statement når man redigere en resource

    public boolean updateResourceType(ArrayList<RessourceType> listOfRessourceTypes, Connection conn) throws SQLException {
        int rowsUpdated = 0;
        String SQLString = "update ressourcetypes "
                + "set RNO = ?, RESSOURCENAME = ?, RVALUE = ?"
                + "where RNO = ?";
        try {
            PreparedStatement statement = null;
            statement = conn.prepareStatement(SQLString);

            for (RessourceType r : listOfRessourceTypes) {
                statement.setInt(1, r.getRNO());
                statement.setString(2, r.getRessourceName());
                statement.setInt(3, r.getrValue());
                statement.setInt(4, r.getRNO());
                rowsUpdated += statement.executeUpdate();
            }

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - updateResourceType");
            System.out.println(e);
        }

        return (rowsUpdated == listOfRessourceTypes.size());
    }
//Metode til at genere SQL Statement når man sletter en resource

    public boolean deleteResourceType(int rno, Connection conn) throws SQLException {

        String SQLString1 = "delete from WAREHOUSE where RNO = ?";
        String SQLString2 = "DELETE FROM RESSOURCETYPES WHERE RNO = ?";
        PreparedStatement statement1 = null;
        PreparedStatement statement2 = null;

        try {
            statement1 = conn.prepareStatement(SQLString1);
            statement1.setInt(1, rno);
            statement1.execute();
            statement2 = conn.prepareStatement(SQLString2);
            statement2.setInt(1, rno);
            statement2.execute();

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - deleteResourceType");
            System.out.println(e);
            return false;
        }

        return true;
    }
//Metode til at genere SQL Statement når man opretter et team

    public boolean insertTeam(ArrayList<Team> listOfTeams, Connection conn) throws SQLException {
        int rowsInserted = 0;
        String SQLString = "insert into teams values (?,?,?)";
        PreparedStatement statement = null;
        statement = conn.prepareStatement(SQLString);

        for (Team t : listOfTeams) {
            statement.setInt(1, t.getTNO());
            statement.setString(2, t.getTeamName());
            statement.setInt(3, t.getTruckNO());
            rowsInserted += statement.executeUpdate();
        }

        return (rowsInserted == listOfTeams.size());
    }
//Metode til at genere SQL Statement når man redigere et team

    public boolean updateTeam(ArrayList<Team> listOfTeams, Connection conn) throws SQLException {
        int rowsUpdated = 0;
        String SQLString = "update teams "
                + "set TNO = ?, TEAMNAME = ?, TRUCKNO = ? "
                + "where TNO = ? ";
        try {
            PreparedStatement statement = null;
            statement = conn.prepareStatement(SQLString);

            for (Team t : listOfTeams) {
                statement.setInt(1, t.getTNO());
                statement.setString(2, t.getTeamName());
                statement.setInt(3, t.getTruckNO());
                statement.setInt(4, t.getTNO());
                rowsUpdated += statement.executeUpdate();
            }

        } catch (Exception e) {
            System.out.println("fail in updateTeam");
            System.out.println(e);

        }

        return (rowsUpdated == listOfTeams.size());
    }
//Metode til at genere SQL Statement når man slette et team

    public boolean deleteTeam(int tno, Connection conn) throws SQLException {

        String SQLString1 = "DELETE FROM TEAMS WHERE TNO = ?";
        PreparedStatement statement = null;

        try {
            statement = conn.prepareStatement(SQLString1);
            statement.setInt(1, tno);
            statement.executeQuery();

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - deleteTeam");
            System.out.println(e);
            return false;
        }

        return true;
    }
//Metode til at genere SQL Statement når man skal tilføje resourcer til en order

    public boolean createResourceBooking(ArrayList<ResourceBooking> listOfRessources, Connection conn) throws SQLException {
        int rowsInserted = 0;
        String SQLString = "insert into RESOURCEBOOKINGS (amount, rno, wno, ono) "
                + "values (?,?,?,?)";
        PreparedStatement statement = null;
        statement = conn.prepareStatement(SQLString);

        for (ResourceBooking or : listOfRessources) {
            statement.setInt(1, or.getAmount());
            statement.setInt(2, or.getRNO());
            statement.setInt(3, or.getWNO());
            statement.setInt(4, or.getONO());
            rowsInserted += statement.executeUpdate();
        }

        return (rowsInserted == listOfRessources.size());
    }
//Metode til at genere SQL Statement når man skal ændre resourcer til en order

    public boolean updateResourceBooking(ArrayList<ResourceBooking> listOfRessources, Connection conn) throws SQLException {
        int rowsUpdated = 0;
        String SQLString = "update RESOURCEBOOKINGS "
                + "set ONO = ?, WNO = ?, RNO = ?, AMOUNT = ? "
                + "where ONO = ? and RNO = ?";
        try {
            PreparedStatement statement = null;
            statement = conn.prepareStatement(SQLString);

            for (ResourceBooking or : listOfRessources) {
                statement.setInt(1, or.getONO());
                statement.setInt(2, or.getWNO());
                statement.setInt(3, or.getRNO());
                statement.setInt(4, or.getAmount());
                statement.setInt(5, or.getONO());
                statement.setInt(6, or.getRNO());
                rowsUpdated += statement.executeUpdate();
            }

        } catch (Exception e) {
            System.out.println("fail in updateResourceBooking");
            System.out.println(e);
        }

        return (rowsUpdated == listOfRessources.size());
    }
//Metode til at genere SQL Statement når man skal slette resourcer til en order

    public boolean deleteResourceBooking(ArrayList<ResourceBooking> listOfRessources, Connection conn) throws SQLException {

        String SQLString = "DELETE FROM RESOURCEBOOKINGS WHERE RNO = ? AND ONO = ? ";
        try {
            PreparedStatement statement = null;
            statement = conn.prepareStatement(SQLString);

            for (ResourceBooking or : listOfRessources) {
                statement.setInt(1, or.getRNO());
                statement.setInt(2, or.getONO());
                statement.execute();
            }

        } catch (Exception e) {
            System.out.println("fail in om.deleteResourceBooking");
            System.out.println(e);
            return false;
        }

        return true;
    }
//Metode til at genere SQL Statement når man skal hente en order

    public Order getOrder(int ono, Connection conn) {
        Order o = null;
        String SQLString1
                = "select ONO, DEPOSITE, DEPOSITEPRICE, SETUPSTARTDATE, SETUPENDDATE, DISMANTLINGSTARTDATE, DISMANTLINGENDDATE, CNO, TNO, ADDRESS, V "
                + "from orders "
                + "where ono = ?";
        PreparedStatement statement = null;

        try {
            statement = conn.prepareStatement(SQLString1);
            statement.setInt(1, ono);
            ResultSet rs = statement.executeQuery();

            if (rs.next()) {
                o = new Order(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDate(4),
                        rs.getDate(5),
                        rs.getDate(6),
                        rs.getDate(7),
                        rs.getInt(8),
                        rs.getInt(11));
            }

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - getOrder");
            System.out.println(e);
        }

        return o;
    }
//Metode til at genere SQL Statement når man skal hente alle orders

    public ArrayList<Order> getAllOrders(Connection conn) throws SQLException {
        ArrayList<Order> orderList = new ArrayList<Order>();

        String SQLString
                = "select ONO, DEPOSITE, DEPOSITEPRICE, SETUPSTARTDATE, SETUPENDDATE, DISMANTLINGSTARTDATE, DISMANTLINGENDDATE, CNO, TNO, ADDRESS, V "
                + "from orders ";
        PreparedStatement statement = null;
        statement = conn.prepareStatement(SQLString);

        ResultSet rs = statement.executeQuery();
        try {
            while (rs.next()) {
                Order o = new Order(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDate(4),
                        rs.getDate(5),
                        rs.getDate(6),
                        rs.getDate(7),
                        rs.getInt(8),
                        rs.getInt(11));

                orderList.add(o);
            }
        } catch (Exception e) {
            System.out.println("Error in om.getAllOrders() - table ORDERS is probably empty");
        }

        return orderList;
    }
//Metode til at genere SQL Statement når man skal hente alle customers

    public ArrayList<Customer> getAllCustomers(Connection conn) throws SQLException {
        ArrayList<Customer> customerList = new ArrayList<Customer>();

        String SQLString
                = "select CNO, CUSTOMERNAME, EMAIL, PHONE, ADDRESS "
                + "from customers ";
        PreparedStatement statement = null;
        statement = conn.prepareStatement(SQLString);

        ResultSet rs = statement.executeQuery();

        while (rs.next()) {
            int CNO = rs.getInt(1);
            String name = rs.getString(2);
            String mail = rs.getString(3);
            String phone = rs.getString(4);
            String address = rs.getString(5);

            Customer c = new Customer(CNO, name, mail, phone, address);

            customerList.add(c);
        }

        return customerList;
    }
//Metode til at genere SQL Statement når man skal hente alle resources

    public ArrayList<RessourceType> getAllRessources(Connection conn) throws SQLException {
        ArrayList<RessourceType> ressourceList = new ArrayList<RessourceType>();

        String SQLString
                = "select RESSOURCETYPES.RNO, RESSOURCENAME, RVALUE, WAREHOUSE.AMOUNT "
                + "from RESSOURCETYPES "
                + "left join WAREHOUSE "
                + "on WAREHOUSE.RNO = RESSOURCETYPES.RNO";
        PreparedStatement statement = null;
        statement = conn.prepareStatement(SQLString);

        ResultSet rs = statement.executeQuery();

        while (rs.next()) {
            RessourceType r = new RessourceType(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getInt(3),
                    rs.getInt(4));

            ressourceList.add(r);
        }

        return ressourceList;
    }
//Metode til at genere SQL Statement når man skal hente alle teams

    public ArrayList<Team> getAllTeams(Connection conn) throws SQLException {
        ArrayList<Team> teamList = new ArrayList<Team>();

        String SQLString
                = "select * "
                + "from teams ";
        PreparedStatement statement = null;
        statement = conn.prepareStatement(SQLString);

        ResultSet rs = statement.executeQuery();

        while (rs.next()) {
            int TNO = rs.getInt(1);
            String name = rs.getString(2);
            int truckNO = rs.getInt(3);

            Team t = new Team(TNO, name, truckNO);

            teamList.add(t);
        }

        return teamList;
    }
//Metode til at genere SQL Statement når man skal oprette en ny customer

    public boolean createCustomer(ArrayList<Customer> listOfCustomers, Connection conn) throws SQLException {
        int rowsInserted = 0;
        String SQLString = "insert into customers values (?,?,?,?,?)";
        PreparedStatement statement = null;
        statement = conn.prepareStatement(SQLString);

        for (Customer c : listOfCustomers) {
            statement.setInt(1, c.getCNO());
            statement.setString(2, c.getCustomerName());
            statement.setString(3, c.getEmail());
            statement.setString(4, c.getPhoneNo());
            statement.setString(5, c.getAddress());
            rowsInserted += statement.executeUpdate();
        }
        return (rowsInserted == listOfCustomers.size());
    }
//Metode til at genere SQL Statement når man skal redigere en customer

    public boolean updateCustomer(ArrayList<Customer> listOfCustomers, Connection conn) throws SQLException {
        int rowsUpdated = 0;
        String SQLString = "update customers "
                + "set CNO = ?, CUSTOMERNAME = ?, EMAIL = ?, PHONE = ?, ADDRESS = ?"
                + "where CNO = ?";
        try {
            PreparedStatement statement = null;
            statement = conn.prepareStatement(SQLString);

            for (Customer c : listOfCustomers) {
                statement.setInt(1, c.getCNO());
                statement.setString(2, c.getCustomerName());
                statement.setString(3, c.getEmail());
                statement.setString(4, c.getPhoneNo());
                statement.setString(5, c.getAddress());
                statement.setInt(6, c.getCNO());
                rowsUpdated += statement.executeUpdate();
            }

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - updateCustomer");
            System.out.println(e);

        }

        return (rowsUpdated == listOfCustomers.size());
    }
//Metode til at genere SQL Statement når man skal slette en customer

    public boolean deleteCustomer(int CNO, Connection conn) throws SQLException {

        String SQLString1 = "DELETE FROM CUSTOMERS WHERE CNO = ?";
        PreparedStatement statement = null;

        try {
            statement = conn.prepareStatement(SQLString1);
            statement.setInt(1, CNO);
            statement.executeQuery();

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - deleteCustomer");
            System.out.println(e);
            return false;
        }

        return true;
    }
//Metode til at generere RNO

    public int getNextRessourceTypeNo(Connection conn) {
        int nextRessourceType = 0;
        String SQLString = "select customerseq.nextval " + "from dual";
        PreparedStatement statement = null;

        try {
            statement = conn.prepareStatement(SQLString);
            ResultSet rs = statement.executeQuery();

            if (rs.next()) {
                nextRessourceType = rs.getInt(1);
            }

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - getNextRessourceTypeNo");
            System.out.println(e.getMessage());
        }
        return nextRessourceType;
    }
//metodetil at generere TNO

    public int getNextTeamNo(Connection conn) {
        int nextTno = 0;
        String SQLString = "select customerseq.nextval " + "from dual";
        PreparedStatement statement = null;

        try {
            statement = conn.prepareStatement(SQLString);
            ResultSet rs = statement.executeQuery();

            if (rs.next()) {
                nextTno = rs.getInt(1);
            }

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - getNextTeamNo");
            System.out.println(e.getMessage());
        }
        return nextTno;
    }
//metodetil at generere CNO

    public int getNextCustomerNo(Connection conn) {
        int nextCno = 0;
        String SQLString = "select customerseq.nextval " + "from dual";
        PreparedStatement statement = null;

        try {
            statement = conn.prepareStatement(SQLString);
            ResultSet rs = statement.executeQuery();

            if (rs.next()) {
                nextCno = rs.getInt(1);
            }

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - getNextCustomerNo");
            System.out.println(e.getMessage());
        }
        return nextCno;
    }
//metodetil at generere ONO

    public int getNextOrderNo(Connection conn) {
        int nextOno = 0;
        String SQLString = "select orderseq.nextval " + "from dual";
        PreparedStatement statement = null;

        try {
            statement = conn.prepareStatement(SQLString);
            ResultSet rs = statement.executeQuery();

            if (rs.next()) {
                nextOno = rs.getInt(1);
            }

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - getNextOrderNo");
            System.out.println(e.getMessage());
        }
        return nextOno;
    }
//metodetil at generere RNO

    public int getNextRessourceNo(Connection conn) {
        int nextRessourceNo = 0;
        String SQLString = "select orderseq.nextval  " + "from dual";
        PreparedStatement statement = null;

        try {
            statement = conn.prepareStatement(SQLString);
            ResultSet rs = statement.executeQuery();

            if (rs.next()) {
                nextRessourceNo = rs.getInt(1);
            }

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - getNextRessourceNo");
            System.out.println(e.getMessage());
        }
        return nextRessourceNo;
    }
//metode der laver SQL statement til at tjekke om resourcer er ledige

    public boolean checkAva(Connection conn, int rno, double amount, Date startDate, Date endDate) {
        int firstValue = 0;
        int secondValue = 0;
        String SQLString = "SELECT amount FROM WAREHOUSE WHERE RNO= ? ";
        PreparedStatement statement = null;

        try {
            statement = conn.prepareStatement(SQLString);
            statement.setInt(1, rno);
            ResultSet rs = statement.executeQuery();

            if (rs.next()) {
                firstValue = rs.getInt(1);

            }

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - AVA Error first");
            System.out.println(e.getMessage());
        }

        String SQLString1 = "SELECT rno , SUM(RESOURCEBOOKINGS.AMOUNT)  FROM RESOURCEBOOKINGS "
                + "LEFT JOIN ORDERS "
                + " ON ORDERS.ONO = RESOURCEBOOKINGS.ONO "
                + " WHERE RNO = ? AND SETUPSTARTDATE > ? AND DISMANTLINGENDDATE < ? OR "
                + " RNO = ? AND    SETUPSTARTDATE < ? AND DISMANTLINGENDDATE > ? OR "
                + " RNO = ? AND    SETUPSTARTDATE > ? AND DISMANTLINGENDDATE > ? OR "
                + " RNO = ? AND   SETUPSTARTDATE < ? AND DISMANTLINGENDDATE < ? "
                + " GROUP BY RNO ";

        PreparedStatement statement1 = null;

        try {
            statement1 = conn.prepareStatement(SQLString1);
            statement1.setInt(1, rno);
            statement1.setDate(2, startDate);
            statement1.setDate(3, endDate);
            statement1.setInt(4, rno);
            statement1.setDate(5, startDate);
            statement1.setDate(6, endDate);
            statement1.setInt(7, rno);
            statement1.setDate(8, startDate);
            statement1.setDate(9, endDate);
            statement1.setInt(10, rno);
            statement1.setDate(11, startDate);
            statement1.setDate(12, endDate);
            ResultSet rs1 = statement1.executeQuery();

            if (rs1.next()) {

                secondValue = rs1.getInt(2);

                secondValue = rs1.getInt(1);

            }

        } catch (Exception e) {
            System.out.println("Fail in OrderMapper - AVA Error first");
            System.out.println(e.getMessage());
        }

        int sum = firstValue - secondValue;

        int finalSum = (int) (sum - amount);

        if (finalSum >= 0) {
            return true;
        } else {
            return false;
        }
    }
//Metode til hente alt fra databasen og lave en printlist

    public ArrayList<String[]> getPrintList(int ono, Connection conn) {
        String SQLString = "SELECT RESOURCEBOOKINGS.RNO, RESSOURCENAME, RESOURCEBOOKINGS.AMOUNT, RESOURCEBOOKINGS.WNO FROM RESOURCEBOOKINGS "
                + "LEFT JOIN  RESSOURCETYPES "
                + "on  RESSOURCETYPES.RNO = RESOURCEBOOKINGS.RNO "
                + "WHERE RESOURCEBOOKINGS.ONO = ?";
        PreparedStatement statement = null;
        ArrayList<String[]> printArray = new ArrayList<String[]>();

        try {
            statement = conn.prepareStatement(SQLString);
            statement.setInt(1, ono);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                String[] ressourceStringArray = {ono + "", rs.getInt(1) + "", rs.getString(2), rs.getInt(3) + "", rs.getInt(4) + ""};
                printArray.add(ressourceStringArray);
            }

        } catch (SQLException e) {
            System.out.println("Error in om.getPrintList()");
            System.out.println(e);
        }

        return printArray;
    }
}
