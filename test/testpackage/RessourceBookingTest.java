/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testpackage;

import Domain.Customer;
import Domain.Order;
import Domain.ResourceBooking;
import Domain.RessourceType;
import dataSource.DBFacade;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Michael Overgaard
 */
public class RessourceBookingTest {

    private DBFacade dbFacade;
    private Customer customer;
    private Order firstOrder;
    private Order beforeBetween;
    private Order betweenAfter;
    private Order betweenBetween;
    private Order beforeAfter;
    private int ressourceNo;
    private int ressourceTotalAmount;
    private RessourceType ressource;

    public RessourceBookingTest() {
    }

    @Before
    public void setUp() {
        try {
            dbFacade = DBFacade.getInstance();
        } catch (SQLException ex) {
            Logger.getLogger(RessourceBookingTest.class.getName()).log(Level.SEVERE, null, ex);

        }

        //  Creates the objects needed to test the checkAva() method
        //  new Customer(Customer No, name, mail, phone number as String, address)
        customer = new Customer(500, "testCustomer", "email@email.email", "19191919", "Testvej 1");

        //  new Order(Order No, deposit , depositPrice, Customer No, startDate, endDate)
        //  Note that the startDate and endDate are the dates the customer needs the scaffolding, 
        //  the setup time and dismantling time has been set as 2 days and is calculated in the Order constructor
        firstOrder = new Order(500, "PAID", 9001, customer.getCNO(), "2014-11-10", "2014-11-20");
        beforeBetween = new Order(501, "PAID", 9001, customer.getCNO(), "2014-11-05", "2014-11-15");
        betweenAfter = new Order(502, "PAID", 9001, customer.getCNO(), "2014-11-15", "2014-11-25");
        betweenBetween = new Order(503, "PAID", 9001, customer.getCNO(), "2014-11-12", "2014-11-18");
        beforeAfter = new Order(504, "PAID", 9001, customer.getCNO(), "2014-11-05", "2014-11-25");

        ressourceNo = 500;
        ressourceTotalAmount = 800;

        //  new RessourceType(No, resourceName, value, amount available)
        ressource = new RessourceType(ressourceNo, "testRessource", 200, ressourceTotalAmount);

        //  Crestes the entries in the database
        dbFacade.startNewBusinessTransaction();

        dbFacade.registerNewCustomer(customer);

        dbFacade.registerNewOrder(firstOrder);
        dbFacade.registerNewOrder(beforeBetween);
        dbFacade.registerNewOrder(betweenAfter);
        dbFacade.registerNewOrder(betweenBetween);
        dbFacade.registerNewOrder(beforeAfter);

        dbFacade.registerNewRessourceType(ressource);

        dbFacade.commitBusinessTransaction();
    }

    @Test
    public void testCreateOrder() {
        //  Tests if the orders are created in the database by loading them from the database through dbFacade.getOrder(OrderNumber);
        //  and checking the expected ONO and version number

        firstOrder = dbFacade.getOrder(500);
        beforeBetween = dbFacade.getOrder(501);
        betweenAfter = dbFacade.getOrder(502);
        betweenBetween = dbFacade.getOrder(503);
        beforeAfter = dbFacade.getOrder(504);

        //  When an order is first created it gets a version number in the database for use with the Offline Optimistic Lock (only omplemented on orders)
        int expectedVersion = 0;

        //  
        assertTrue(firstOrder.getONO() == 500 && firstOrder.getVersion() == expectedVersion);
        assertTrue(beforeBetween.getONO() == 501 && firstOrder.getVersion() == expectedVersion);
        assertTrue(betweenAfter.getONO() == 502 && firstOrder.getVersion() == expectedVersion);
        assertTrue(betweenBetween.getONO() == 503 && firstOrder.getVersion() == expectedVersion);
        assertTrue(beforeAfter.getONO() == 504 && firstOrder.getVersion() == expectedVersion);
    }

    @Test
    public void testCheckAva() {
        //  Testing the checkAva() method which checks if there is enough ressources in a given timeframe to add to an order

        //  Loading the orders from the DB
        firstOrder = dbFacade.getOrder(500);
        beforeBetween = dbFacade.getOrder(501);
        betweenAfter = dbFacade.getOrder(502);
        betweenBetween = dbFacade.getOrder(503);
        beforeAfter = dbFacade.getOrder(504);

        int amountToBook = 500; //  On all the following we'll be trying to book 500 of the testRessource to the different orders. There is 800 total of that ressource.

        /*  To understand this test:
         The premises on which we build this test is that there are 4 relevant scenarios when trying to book ressources to an order:
        
         1st scenario: The new order starts before and ends during another order
         2nd scenario: The new order starts during and ends after another order
         3rd scenario: The new order both start and end during another order
         4th scenario: The new order starts before and ends after another order
        
         Visual explanation where |--| shows the timerframe of the order:
         firstOrder                                              |-------------|
         1st scenario(beforeBetween)                        |-------|
         2nd scenario(betweenAfter)                                        |------|
         3rd scenario(betweenBetween)                                |----|
         4th scenario(beforeAfter)                        |-----------------------|
        
         The scenarios not tested here are if both the start and end date of an order on the same side of the timeframe of an existing order
         */
        //  This the first order. It should return true because in the timeframe specified there are no other orders using the testRessource
        assertTrue(dbFacade.checkAva(ressourceNo, amountToBook, firstOrder.getSetupStartDate(), firstOrder.getDismantlingEndDate()));

        //  Actually booking the ressources for the firstOrder. The 1 in the ResourceBooking is the warehouse number
        ResourceBooking or = new ResourceBooking(amountToBook, ressourceNo, 1, firstOrder.getONO());
        dbFacade.startNewBusinessTransaction();
        dbFacade.registerNewResourceBooking(or);
        dbFacade.commitBusinessTransaction();

        //  Now, after booking the 500/800 testRessources to firstOrder, following checkAva for 500 testRessources should become false for all of the mentioned scenarios
        assertFalse(dbFacade.checkAva(ressourceNo, amountToBook, beforeBetween.getSetupStartDate(), beforeBetween.getDismantlingEndDate()));
        assertFalse(dbFacade.checkAva(ressourceNo, amountToBook, betweenAfter.getSetupStartDate(), betweenAfter.getDismantlingEndDate()));
        assertFalse(dbFacade.checkAva(ressourceNo, amountToBook, betweenBetween.getSetupStartDate(), betweenBetween.getDismantlingEndDate()));
        assertFalse(dbFacade.checkAva(ressourceNo, amountToBook, beforeAfter.getSetupStartDate(), beforeAfter.getDismantlingEndDate()));

        //  Since 500/800 resources are booked the remaining 300 resources should be able to be booked. 
        //  Since checkAva does not book the resources we can test all the scenarios with the following assertions.
        amountToBook = 300;

        assertTrue(dbFacade.checkAva(ressourceNo, amountToBook, beforeBetween.getSetupStartDate(), beforeBetween.getDismantlingEndDate()));
        assertTrue(dbFacade.checkAva(ressourceNo, amountToBook, betweenAfter.getSetupStartDate(), betweenAfter.getDismantlingEndDate()));
        assertTrue(dbFacade.checkAva(ressourceNo, amountToBook, betweenBetween.getSetupStartDate(), betweenBetween.getDismantlingEndDate()));
        assertTrue(dbFacade.checkAva(ressourceNo, amountToBook, beforeAfter.getSetupStartDate(), beforeAfter.getDismantlingEndDate()));

    }

    @After
    public void cleanUp() throws SQLException {

        //  We can only delete one order at a time from the database at a time, so a loop is necessary to delete them all to return the database to it's former state
        for (int i = 500; i < 505; i++) {
            dbFacade.startNewBusinessTransaction();
            dbFacade.deleteOrder(i);
            dbFacade.commitBusinessTransaction();
        }

        dbFacade.startNewBusinessTransaction();
        dbFacade.deleteRessourceType(500);
        dbFacade.deleteCustomer(500);
        dbFacade.commitBusinessTransaction();
    }
}
